from setuptools import setup, find_packages
from codecs import open
from os import path

__author__ = "Yvonni Tsaknaki"
__license__ = "MIT License"
__email__ = "mathematyvian@gmail.com"


here = path.abspath(path.dirname(__file__))

with open(path.join(here, "README.md"), encoding="utf-8") as f:
    long_description = f.read()

with open(path.join(here, "requirements.txt"), encoding="utf-8") as f:
    requirements = f.read().splitlines()


setup(
    name="OLOFMI",
    version="0.1.0",
    license="MIT License",
    description="A market microstructure package for the computation of market impact funcion of regimes with an online prediction of order flow and market impact",
    url="https://gitlab.com/YvTsak/olofmi",
    author="Yvonni Tsaknaki",
    author_email="mathematyvian@gmail.com",
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Science/Research",
        "License :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
    ],
    keywords="Market Impact function of regimes, Online prediction of order flow, Online prediction of market impact, square-root impact law",
    install_requires=requirements,
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=find_packages(),
    setup_requires=['wheel']
    )


