# OLOFMI/Online Learning of Order Flow and Market Impact

Code for [Online Learning of Order Flow and Market Impact with Bayesian Change-Point Detection Methods](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4500960) by Ioanna-Yvonni Tsaknaki, Fabrizio Lillo, Piero Mazzarisi, 2023.

## Abstract
Financial order flow exhibits a remarkable level of persistence, wherein buy (sell)
trades are often followed by subsequent buy (sell) trades over extended periods.
This persistence can be attributed to the division and gradual execution of large
orders. Consequently, distinct order flow regimes might emerge, which can be identified
through suitable time series models applied to market data. In this paper,
we propose the use of Bayesian online change-point detection (BOCPD) methods
to identify regime shifts in real-time and enable online predictions of order flow and
market impact. To enhance the effectiveness of our approach, we have developed a
novel BOCPD method using a score-driven approach. This method accommodates
temporal correlations and time-varying parameters within each regime. Through
empirical application to NASDAQ data, we have found that: (i) Our newly proposed
model demonstrates superior out-of-sample predictive performance compared
to existing models that assume i.i.d. behavior within each regime; (ii) When examining
the residuals, our model demonstrates good specification in terms of both
distributional assumptions and temporal correlations; (iii) Within a given regime,
the price dynamics exhibit a concave relationship with respect to time and volume,
mirroring the characteristics of actual large orders; (iv) By incorporating regime
information, our model produces more accurate online predictions of order flow and
market impact compared to models that do not consider regimes.

## Installation

Use pip:
```bash
pip install OLOFMI
```



