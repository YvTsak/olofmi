"""
Implementation of sections 5 and 6 from paper

"Online Learning of Order Flow and Market Impact with 
Bayesian Change-Point Detection Methods"   
Tsaknaki,Lillo,Mazzarisi, 2023

see: https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4500960
--------------------------------------------------------------------------
The code computes the following quantities along with its figures: 

1.The market impact function of the order flow regimes - eq.(38)
2.The estimation of the parameters of nonlinear regression - eq.(39) 
3.The (un)conditional predictors for order flow - eq.(42)-(43)
4.The (un)conditional predictors for market impact - eq.(44)-(45)

Author: Yvonni Tsaknaki
"""
import matplotlib.pyplot as plt
from   matplotlib.pyplot import cm    
import numpy as np
import pandas as pd
import statistics
from   scipy.optimize import curve_fit
import math
import os


class CreateData:
    
    def __init__(self, sdb_model, dt_model, g_model, h_model, h, d, q, mean0, var0, \
                 var, cor, lamda, k, pred_steps, n_exec, theta, thresholds, observations):
        
        self.n_bullets           = k
        self.pred_bullets        = pred_steps
        self.n_exec              = n_exec
        self.theta               = theta
        self.thresholds          = thresholds
        self.observations        = [i-1 for i in observations]
        self.sdb_model           = sdb_model
        self.dt_model            = dt_model
        self.g_model             = g_model
        self.h_model             = h_model
        self.h                   = h
        self.d                   = d
        self.q                   = q
        self.mean0               = mean0
        self.var0                = var0
        self.var                 = var
        self.cor                 = cor
        self.lamda               = lamda
        self.mapping             = {}
        self.bullets_all_theta   = {}
        self.pred_prices_reg_all = {}
        self.pred_prices_all     = {}
        self.pred_signs_reg_all  = {}
        self.pred_signs_all      = {}
        self.bullets_all_theta_plot   = {}
        self.pred_prices_reg_all_plot = {}
        self.pred_prices_all_plot     = {}
        self.pred_signs_all_plot      = {}
        self.pred_signs_reg_all_plot  = {}
        self.regime_pc_all            = []
        self.regime_vol_all           = []
        self.__update_mapping()
        for th in self.thresholds:
            self.bullets_all_theta[th] = {}
            self.bullets_all_theta_plot[th] = {}
        for obs in self.observations:
            self.pred_prices_reg_all[obs] = {}
            self.pred_prices_all[obs]     = {}
            self.pred_signs_reg_all[obs]  = {}
            self.pred_signs_all[obs]      = {}
            self.pred_prices_reg_all_plot[obs] = {}
            self.pred_prices_all_plot[obs]     = {}
            self.pred_signs_all_plot[obs]      = {}
            self.pred_signs_reg_all_plot[obs]  = {}
        self.__dataplot()
        
    def __update_mapping(self):
        
        for filename in os.listdir('./data'):
            
            if 'message' in filename:
                print(filename)
                mess_day_df,volm_day_df = self.create_dataset(filename)
                mess_day_df.to_csv(f"./data/{filename[:15]}_msg_{self.n_exec}.csv", sep = ',',index=False)     
                volm_day_df.to_csv(f"./data/{filename[:15]}_vlm_{self.n_exec}.csv", sep = ',',index=False)     
                
                dt = self.dt_model(file_name = f"./data/{filename[:15]}_vlm_{self.n_exec}.csv")
                data = dt.update_data()

                T = len(data)
                hazard = self.h_model(T,self.h)
                
                sdb = self.sdb_model(T,self.d,self.q)
                pr_model = self.g_model(self.mean0,self.var0,self.var,self.lamda,self.q,self.cor)
                _,cps = sdb.bocpd(data, pr_model, hazard, plot = False)

                self.mapping[(f"{filename[:15]}_msg_{self.n_exec}.csv",f"{filename[:15]}_vlm_{self.n_exec}.csv")] = cps
                
        
    def exec_number(self,executions_mess):
        """
        Input : the time series of signed volume
        Output: the aggregated volume according to the number of executions
        """
        sign_vol_nexec = []
        
        for i in range(0,len(executions_mess['Signed Volume']),self.n_exec):
            
            signed_vol = sum(executions_mess.loc[i:i + self.n_exec-1,'Signed Volume'])
            sign_vol_nexec.append(signed_vol)
    
        return sign_vol_nexec
        
    
    def create_dataset(self,msg_name):

        data_mess = pd.read_csv(f"./data/{msg_name}")
        columns_mess = ['Time','Type','ID','Size','Price','Sign','Null']
        data_mess.columns = columns_mess
        data_mess = data_mess.drop(['Null'], axis=1)
        executions_mess = data_mess.loc[(data_mess['Type']==4)]
        executions_mess = executions_mess.reset_index(drop=True)
        executions_mess['Signed Volume'] = -executions_mess['Size']*executions_mess['Sign']
       
        executions_mess_df_day = pd.DataFrame(executions_mess,columns = ['Price','Sign','Signed Volume'])
        volm_single_day = self.exec_number(executions_mess)
        volm_single_day_df = pd.DataFrame(volm_single_day,columns = ['Volume Sum'])
    
        return executions_mess_df_day,volm_single_day_df    

    
    
    def regime_sign_nobs(self,cp_list,volm_data,n_obs):
        """Find the sign of the first n_obs observations for the conditional
            model - see eq.(42)
        """
        regime_sign_init = {}
    
        for i,cp in enumerate(cp_list):
    
            if i == len(cp_list)-1 and cp == len(volm_data):
                continue
            if cp_list[i]==cp_list[-1] and len(volm_data)-cp_list[i]<n_obs-1:
                continue
            if i<len(cp_list)-1 and cp_list[i]+n_obs>=cp_list[i+1]:
                continue
            
            if sum(volm_data.loc[cp_list[i]:cp_list[i]+n_obs,'Volume Sum'])>0:
                regime_sign_init[cp] = 1
            if sum(volm_data.loc[cp_list[i]:cp_list[i]+n_obs,'Volume Sum'])<0:
                regime_sign_init[cp] = -1
            if sum(volm_data.loc[cp_list[i]:cp_list[i]+n_obs,'Volume Sum'])==0:
                print('regime with no sign',i)
    
        return regime_sign_init


    def observation_sign_nobs(self,volm_data,n_obs):
        """Find the sign of the first n_obs observations for the unconditional
            model - see eq.(43)
        """
        fake_cp_list = [i for i in range(0,len(volm_data)-1)] 
        observation_sign_n = {}
        
        for i,cp in enumerate(fake_cp_list):
    
            if i == len(fake_cp_list)-1 and cp==len(volm_data):
                print('The last cp is out of bounds')
                continue
            
            if fake_cp_list[i]==fake_cp_list[-1] and len(volm_data)-fake_cp_list[i]<n_obs-1:
                continue
            
            if sum(volm_data.loc[fake_cp_list[i]:fake_cp_list[i]+n_obs,'Volume Sum'])>0:
                observation_sign_n[cp] = 1
            if sum(volm_data.loc[fake_cp_list[i]:fake_cp_list[i]+n_obs,'Volume Sum'])<0:
                observation_sign_n[cp] = -1
            if sum(volm_data.loc[fake_cp_list[i]:fake_cp_list[i]+n_obs,'Volume Sum'])==0:
                print('regime with no sign',i)
    
            if i == len(fake_cp_list)-1 and fake_cp_list[i]>len(volm_data):
                print('The last cp is out of bounds')
     
        return observation_sign_n
    
    
    def strong_regime_sign(self,cp_list,volm_data,threshold):
        """Find the sign of the regimes that satisfy inequality (37)
        """
        regime_sign_dict = {}
                    
        for i,cp in enumerate(cp_list):
            if i<len(cp_list)-1:
                if np.abs(sum(volm_data.loc[cp_list[i]:cp_list[i+1]-1,'Volume Sum'])/sum(np.abs(volm_data.loc[cp_list[i]:cp_list[i+1]-1,'Volume Sum'])))>threshold:
    
                    if sum(volm_data.loc[cp_list[i]:cp_list[i+1]-1,'Volume Sum'])>0:
                        regime_sign_dict[cp] = 1
          
                    if sum(volm_data.loc[cp_list[i]:cp_list[i+1]-1,'Volume Sum'])<0:
                        regime_sign_dict[cp] = -1
                       
                    if sum(volm_data.loc[cp_list[i]:cp_list[i+1]-1,'Volume Sum'])==0:
                        print('regime with no sign',i)
                else:
                    continue
           
            if i == len(cp_list)-1 and cp_list[i]<=len(volm_data):
                if np.abs(sum(volm_data.loc[cp_list[i]:,'Volume Sum'])/\
                          sum(np.abs(volm_data.loc[cp_list[i]:,'Volume Sum'])))>threshold:
                    if sum(volm_data.loc[cp_list[i]:,'Volume Sum'])>0:
                        regime_sign_dict[cp] = 1
                    if sum(volm_data.loc[cp_list[i]:,'Volume Sum'])<0:
                        regime_sign_dict[cp] = -1
                    if sum(volm_data.loc[cp_list[i]:,'Volume Sum'])==0:
                        print('regime with no sign')
                else:
                    continue
            
            if i == len(cp_list)-1 and cp_list[i]>len(volm_data):
                print('The last cp is out of bounds')
    
        return regime_sign_dict


    def market_impact(self,exec_data,regime_sign_dict):
        """Compute the market impact during the order flow regimes - see eq.(38)
        """
        bullets = {}
        bullets_plot = {}
        cps_list = [cp for cp in regime_sign_dict]
    
        for i in range(1,self.n_bullets+1):
            
            bullets[i] = []
            bullets_plot[i] = []
    
            for j,cp in enumerate(regime_sign_dict):
        
                if j<len(regime_sign_dict)-1:
                    if cps_list[j+1]-cps_list[j]<i:
                        continue
                
                if j == len(regime_sign_dict)-1:
                    if (cps_list[j]+i)*self.n_exec-1>len(exec_data):
                        continue
                # market impact in basis points
                bullets[i].append((10**4)*np.log(exec_data.loc[self.n_exec*(cp+i)-1,'Price']/exec_data.loc[self.n_exec*cp,'Price'])*regime_sign_dict[cp])
    
            if len(bullets[i])>1:
                # the mean
                bullets_plot[i].append(statistics.mean(bullets[i]))
                # the standard error
                bullets_plot[i].append(statistics.stdev(bullets[i])/np.sqrt(len(bullets[i])))
        
        return bullets,bullets_plot   
    
    
    def pred_ofmi(self,exec_data,regime_sign_init,mi,of):
        """Computation of (un)conditional predictors for order flow and market impact
            see eq.(42)-(45)
        """
        bullets = {}
        bullets_plot = {}
        cps_list = [cp for cp in regime_sign_init]
    
        for i in range(1,self.pred_bullets+1):
            
            bullets[i] = []
            bullets_plot[i] = []
    
            for j,cp in enumerate(regime_sign_init):
                
                if (cps_list[j]+i+1)*self.n_exec-1>len(exec_data):
                    continue
                if mi == True:
                    # market impact predictor
                    bullets[i].append((10**4)*np.log(exec_data.loc[self.n_exec*(cp+i+1)-1,'Price']/exec_data.loc[self.n_exec*(cp+i),'Price'])*regime_sign_init[cp])
                if of == True:
                    # order flow predictor
                    bullets[i].append(self.find_sign(sum(exec_data.loc[self.n_exec*(cp+i):self.n_exec*(cp+i+1)-1,'Signed Volume']))*regime_sign_init[cp])

            if len(bullets[i])>1:
                # the mean
                bullets_plot[i].append(statistics.mean(bullets[i]))
                # the standard error
                bullets_plot[i].append(statistics.stdev(bullets[i])/np.sqrt(len(bullets[i])))
              
        return bullets,bullets_plot


    def find_sign(self,x):
        
        if x>0:
            return 1
        elif x<0:
            return -1
        else:
            return 0


    def append_list2list(self,list_new,list_old):
    
        for i in range(len(list_old)):
            list_new.append(list_old[i])
        
        return list_new


    def append_dict2dict(self,dict_new,dict_old,n_keys,count):

        for key in range(1,n_keys+1):
         
            if count == 0:
                dict_new[key] = dict_old[key]
            
            if count>0:
                self.append_list2list(dict_new[key],dict_old[key])
            
        return dict_new
    
    
    def shift_l(self,l):
        
        trsf_l = [l[0]]
        
        for i in range(1,len(l)):
            
            if l[i] == 1:
                continue
            
            trsf_l.append(l[i]-1)
        
        return trsf_l
       

    def regime_volume(self,cp_list_original,volm_data,regime_sign_dict):
        """Executed volume during order flow regimes - see eq.(39)
        """
        regime_vol = []
        cp_list = [cp for cp in regime_sign_dict]
        
        for i,cp in enumerate(cp_list_original):
            
            if i<len(cp_list_original)-1 and (cp in cp_list):
                regime_vol.append(sum(volm_data.loc[cp_list_original[i]:cp_list_original[i+1]-1,'Volume Sum'])*regime_sign_dict[cp])
            
            if i == len(cp_list_original)-1 and cp_list_original[i]<=len(volm_data):
                break
            
            if i == len(cp_list_original)-1 and cp_list_original[i]>len(volm_data):
                break
       
        return regime_vol
    
    
    def regime_price_change(self,cp_list_original,exec_data,regime_sign_dict):
        """Market Impact during order flow regimes - see eq.(39)
        """
        p_change = []
        cp_list = [cp for cp in regime_sign_dict]
      
        for j,cp in enumerate(cp_list_original):
            
            if j<len(cp_list_original)-1 and (cp in cp_list):
                p_change.append(np.log(exec_data.loc[self.n_exec*(cp_list_original[j+1])-1,'Price']/exec_data.loc[self.n_exec*cp,'Price'])*regime_sign_dict[cp])
    
            if j==len(cp_list_original)-1:
                break
        
        return p_change


    def fun_opt(self,volume,A,gamma):
        x = A*(volume**gamma) 
        return x
    
    
    def multply_list(self,lista,number):
        new_lista = []
        
        for i in lista:
            new_lista.append(i*number)
            
        return new_lista

    
    def data_thetas(self,mess_day_df,vol_day_df,cp_list,count):
        """Create data to study the market impact during order flow regimes - section 5
        """
        bullets_theta = {}
        cp_list = self.shift_l(cp_list)
        
        for threshold in self.thresholds:
            
            regime_sign_dict_theta = self.strong_regime_sign(cp_list,vol_day_df,threshold)
            bullets_theta = self.market_impact(mess_day_df,regime_sign_dict_theta)[0]
            self.append_dict2dict(self.bullets_all_theta[threshold],bullets_theta,self.n_bullets,count)
            
            if threshold == self.theta:
                
                regime_pc = self.regime_price_change(cp_list,mess_day_df,regime_sign_dict_theta)
                regime_vol = self.regime_volume(cp_list,vol_day_df,regime_sign_dict_theta)
        
        self.append_list2list(self.regime_pc_all, regime_pc)
        self.append_list2list(self.regime_vol_all, regime_vol)
        

    def round_down(self,n, decimals=0):
        multiplier = 10 ** decimals
        return math.floor(n * multiplier) / multiplier
    
    
    def nonlinear_regression(self):
        """Estimation of the parameters of the nonlinear regression - Eq.(39)
           with and without outliers.
        """
        self.regime_pc_all = self.multply_list(self.regime_pc_all,10**4)
        regime_pc_all_outliers = list(self.regime_pc_all)  
        regime_vol_all_outliers = list(self.regime_vol_all)    
        
        genericParameters = [0.00001,0.5]
        fittedParameters_outliers, pcov_outliers = curve_fit(self.fun_opt, regime_vol_all_outliers, regime_pc_all_outliers, genericParameters,method='dogbox',bounds = (0,1))
        modelPredictions_outliers = self.fun_opt(regime_vol_all_outliers, *fittedParameters_outliers) 
        
        absError_outliers = modelPredictions_outliers - regime_pc_all_outliers
        Rsquared_outliers = 1.0 - (np.var(absError_outliers) / np.var(regime_pc_all_outliers))
        R_rounded_outliers = self.round_down(Rsquared_outliers,2)
        
        # remove outliers with the interquartile approach - see pg.24
        Q1 = np.percentile(self.regime_pc_all, 25, method='midpoint')
        Q3 = np.percentile(self.regime_pc_all, 75, method='midpoint')
        IQR = Q3 - Q1
        lower = Q1 - 1.5*IQR
        upper = Q3 + 1.5*IQR
        
        unwanted = []
        
        for i,j in enumerate(self.regime_pc_all.copy()):
            
            if j>=upper or j<=lower:
                unwanted.append(i)
                self.regime_pc_all.remove(j)
        cnt = 0  
        
        for i in range(len(unwanted)):
            self.regime_vol_all.remove(self.regime_vol_all[unwanted[i]-cnt])
            cnt+=1
    
        fittedParameters, pcov = curve_fit(self.fun_opt, self.regime_vol_all, self.regime_pc_all, genericParameters,method='dogbox',bounds = (0,1))
        modelPredictions = self.fun_opt(self.regime_vol_all, *fittedParameters) 
        absError = modelPredictions - self.regime_pc_all
        Rsquared = 1.0 - (np.var(absError) / np.var(self.regime_pc_all))
        R_rounded = self.round_down(Rsquared,2)
        
        results = [[fittedParameters_outliers[0],np.sqrt(np.diag(pcov_outliers))[0],fittedParameters_outliers[1],np.sqrt(np.diag(pcov_outliers))[1]],
        [fittedParameters[0],np.sqrt(np.diag(pcov))[0],fittedParameters[1],np.sqrt(np.diag(pcov))[1]]]
        results_df = pd.DataFrame(results,columns = ['A','SE of A',r'$\gamma $',r'SE of $\gamma$'],index = ['with outliers','without outliers'])
        print(results_df)
        
        return fittedParameters,fittedParameters_outliers,R_rounded,R_rounded_outliers,regime_pc_all_outliers,regime_vol_all_outliers
      
        
    def signs_obs(self,mess_day_df,vol_day_df,cp_list,count):
        """Create data to make online predictions of order flow and market impact
            section 6
        """
        cp_list = self.shift_l(cp_list)

        for obs in self.observations:
            
            sign_reg_day = self.regime_sign_nobs(cp_list,vol_day_df,obs)
            sign_day = self.observation_sign_nobs(vol_day_df,obs)
            
            pred_prices_reg_day = self.pred_ofmi(mess_day_df,sign_reg_day,mi=True,of=False)[0]
            pred_prices_day = self.pred_ofmi(mess_day_df,sign_day,mi=True,of=False)[0]
            pred_signs_reg_day = self.pred_ofmi(mess_day_df,sign_reg_day,mi=False,of=True)[0]
            pred_signs_day = self.pred_ofmi(mess_day_df,sign_day,mi=False,of=True)[0]
            
            self.append_dict2dict(self.pred_prices_reg_all[obs],pred_prices_reg_day,self.pred_bullets,count)
            self.append_dict2dict(self.pred_prices_all[obs],pred_prices_day,self.pred_bullets,count)
            self.append_dict2dict(self.pred_signs_reg_all[obs],pred_signs_reg_day,self.pred_bullets,count)
            self.append_dict2dict(self.pred_signs_all[obs],pred_signs_day,self.pred_bullets,count)
                
    
    def __dict_for_plot(self,bull_dict,n_bull):
        
        bull_plot_dict = {}
        
        for i in range(1,n_bull+1):
            
            bull_plot_dict[i] = []
            bull_plot_dict[i].append(statistics.mean(bull_dict[i]))
            bull_plot_dict[i].append(statistics.stdev(bull_dict[i])/np.sqrt(len(bull_dict[i])))
        
        return bull_plot_dict
    
    
    def __dataplot(self):
        """Create the dataset for all the trading days available for the stock of study.
            Prepare these datasets for ploting.
        """
        # if fig5 == True:
        #     print("Market Impact function for regimes is computing...")
        
        for count,filenames in enumerate(self.mapping):

            mess_day_df = pd.read_csv(f"./data/{filenames[0]}")
            vol_day_df  = pd.read_csv(f"./data/{filenames[1]}")
            
            # if fig5 == True or fig8 == True:
            self.data_thetas(mess_day_df,vol_day_df,self.mapping[filenames],count)
            
            # if fig6 == True or fig7 == True:
                
            if count == 0:
                print("Data is preparing for online predictions...")
            
            self.signs_obs(mess_day_df,vol_day_df,self.mapping[filenames],count)
                
        # if fig5 == True:
            
            for threshold in self.thresholds:
                self.bullets_all_theta_plot[threshold] = self.__dict_for_plot(self.bullets_all_theta[threshold],self.n_bullets)
        
        # if fig6 == True or fig7 == True:
            
            for obs in self.observations:
                self.pred_prices_reg_all_plot[obs] = self.__dict_for_plot(self.pred_prices_reg_all[obs],self.pred_bullets)
                self.pred_prices_all_plot[obs] = self.__dict_for_plot(self.pred_prices_all[obs],self.pred_bullets)
                self.pred_signs_reg_all_plot[obs] = self.__dict_for_plot(self.pred_signs_reg_all[obs],self.pred_bullets)
                self.pred_signs_all_plot[obs] = self.__dict_for_plot(self.pred_signs_all[obs],self.pred_bullets)
        

    def __plt_sqrt_impact_law(self, plot = False):
        """Plot fig8(pg 25)
        """
        fittedParameters,fittedParameters_outliers,Rsquared,Rsquared_outliers,regime_pc_all_outliers,regime_vol_all_outliers = self.nonlinear_regression()
        
        if plot == True:
            
            f,axes = plt.subplots(1,1,figsize=(7, 3), dpi=100)
            
            axes.set_title(' with $\Delta t=3$ min and $\Theta=$'+str(self.theta),fontsize=13)
            axes.plot(regime_vol_all_outliers, regime_pc_all_outliers,  'o',markersize=4, color = 'red')
            axes.plot(self.regime_vol_all, self.regime_pc_all, 'o',markersize=4)
        
            vol_outliers = np.linspace(min(regime_vol_all_outliers), max(regime_vol_all_outliers))
            prc_outliers = self.fun_opt(vol_outliers, *fittedParameters_outliers)
            vol = np.linspace(min(self.regime_vol_all), max(self.regime_vol_all))
            prc = self.fun_opt(vol, *fittedParameters)
            
            axes.plot(vol_outliers, prc_outliers, color = 'limegreen')
            axes.plot(vol, prc)
            axes.set_xlabel(r'$\epsilon_R\sum_{t_R\leq t<s_R}x_t$',fontsize=13) 
            axes.set_ylabel(r'$\epsilon_R\Delta p_R$',fontsize=13) 
            axes.text(0.92, 0.17, '$R^2=$'+str(Rsquared_outliers),
                verticalalignment='bottom', horizontalalignment='right',
                transform=axes.transAxes,
                color='limegreen', fontsize=13)
            axes.text(0.92, 0.08, '$R^2=$'+str(Rsquared),
                verticalalignment='bottom', horizontalalignment='right',
                transform=axes.transAxes,
                color='darkorange', fontsize=13)
            axes.tick_params(labelsize=10)    


    def __plt_market_impact(self,bulls_plot_all):
        """Plot fig5(pg 15)
        """
        colors = cm.rainbow(np.linspace(0, 1, len(self.thresholds)))
        std_error_all = {}
        mean_all = {}
        
        fig, axes = plt.subplots(1, 1, figsize=(4,5))
        plt.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=0.5, hspace=0.5)
        ax2 = axes
        ticks = [i for i in range(1,self.n_bullets+1)]
        tick_labels = [str(i) for i in ticks]
            
        for j,threshold in enumerate(self.thresholds):
            std_error = []
            mean = []
            
            for i in range(1,self.n_bullets+1):
                
                std_error.append(bulls_plot_all[threshold][i][1])
                mean.append(bulls_plot_all[threshold][i][0])
            
            std_error_all[threshold] = std_error 
            mean_all[threshold] = mean
            
            ax2.errorbar(range(1,self.n_bullets+1),mean_all[threshold], std_error_all[threshold], fmt='o', linewidth=1,color = colors[j], capsize=6, label = f"$\Theta = ${threshold}")
            ax2.scatter(range(1,self.n_bullets+1),mean_all[threshold],s=30,color = colors[j])
            ax2.plot(range(1,self.n_bullets+1),mean_all[threshold],linewidth=1,color = colors[j])
            
        ax2.plot(range(1,self.n_bullets+1), [0 for i in range(self.n_bullets)], c='black', ls='dashed',linewidth=2)
        ax2.set_title("sth",fontsize=16,style='italic',y=1)
        ax2.set_xlabel('$k$', fontsize=16)
        ax2.tick_params(labelsize=10)
        ax2.set_ylabel("$\mathcal{I}^{\Theta}(k)$",fontsize=16)
        ax2.set_xticks(ticks)
        ax2.set_xticklabels(tick_labels)
        ax2.grid()
        ax2.legend(fontsize="12")
        
    
    def __plt_online_predictions(self,pred_reg_all_plot,pred_all_plot,price,sign):
        """Plot fig6(pg 18) and fig7(pg 19)
        """
    
        colors = cm.rainbow(np.linspace(0, 1, self.pred_bullets))
        std_error_reg_all = {}
        mean_reg_all = {}
        std_error_all = {}
        mean_all = {}
        
        fig, axis = plt.subplots(1, 1, figsize=(4,5))
        plt.subplots_adjust(left=None, bottom=0.15, right=None, top=0.91, wspace=0.3, hspace=0.35)
        ax1 = axis
        
        ticks = [i for i in range(1,self.pred_bullets+1)]
        tick_labels = [str(i) for i in ticks]
        
        for j,obs in enumerate(self.observations):
            
            std_error_reg = []
            mean_reg = []
            std_error = []
            mean = []
            mn_reg = 0
            mn = 0
            
            for i in range(1,self.pred_bullets+1):
                
                if sign == True:
                    
                    std_error_reg.append(pred_reg_all_plot[obs][i][1])
                    mean_reg.append(pred_reg_all_plot[obs][i][0])
                    std_error.append(pred_all_plot[obs][i][1])
                    mean.append(pred_all_plot[obs][i][0])
                
                if price == True:
                    
                    std_error_reg.append(pred_reg_all_plot[obs][i][1])
                    mn_reg =mn_reg+pred_reg_all_plot[obs][i][0]
                    mean_reg.append(mn_reg)
                    std_error.append(pred_all_plot[obs][i][1])
                    mn += pred_all_plot[obs][i][0]
                    mean.append(mn)
            
            std_error_reg_all[obs] = std_error_reg 
            mean_reg_all[obs] = mean_reg
            std_error_all[obs] = std_error 
            mean_all[obs] = mean
            
            if sign == True:
                
                label1 = "$\mathcal{I}_{\epsilon}^{("+str(obs+1)+")}(k)$"
                label2 = "$\mathcal{\\tilde{I}}_{\epsilon}^{("+str(obs+1)+")}(k)$"
            
            if price == True:
                
                label1 = "$\mathcal{I}_{\Delta p}^{("+str(obs+1)+")}(k)$"
                label2 = "$\mathcal{\\tilde{I}}_{\Delta p}^{("+str(obs+1)+")}(k)$"
        
            ax1.errorbar(range(1,self.pred_bullets+1),mean_reg_all[obs], std_error_reg_all[obs], fmt='s', linewidth=1, capsize=6,color = colors[j])
            ax1.scatter(range(1,self.pred_bullets+1),mean_reg_all[obs],s=30,marker='s',color = colors[j])
            ax1.plot(range(1,self.pred_bullets+1),mean_reg_all[obs],color = colors[j],linewidth=1,label=label1)
            
            ax1.errorbar(range(1,self.pred_bullets+1),mean_all[obs], std_error_all[obs], fmt='s', linewidth=1, capsize=6, color=colors[j])
            ax1.scatter(range(1,self.pred_bullets+1),mean_all[obs],s=30,marker='s',color=colors[j])
            ax1.plot(range(1,self.pred_bullets+1),mean_all[obs],color=colors[j],linewidth=1,ls='dashed',label=label2)
        
        if sign == True:
            ax1.set_ylabel(r'$\mathcal{I}_{\epsilon}(k),\mathcal{\tilde{I}}_{\epsilon}(k)$',fontsize=16)
        
        if price == True:
            ax1.set_ylabel(r'$\mathcal{I}_{\Delta p}(k),\mathcal{\tilde{I}}_{\Delta p}(k)$',fontsize=16)
        
        ax1.legend(loc="lower center",ncol = 2,fontsize="12")
        ax1.set_title("sth",fontsize=16,style='italic',y=1)
        ax1.set_xlabel("$k$",fontsize=16)
        ax1.tick_params(labelsize=10)
        ax1.set_xticks(ticks)
        ax1.set_xticklabels(tick_labels)
        ax1.grid()
    
    
    def get_market_impact(self, plot = False):
        
        if plot == True:
            self.__plt_market_impact(self.bullets_all_theta_plot)
            
        return self.bullets_all_theta_plot
    
    
    def get_online_pred_sign(self, plot = False):
        
        if plot == True:
            self.__plt_online_predictions(self.pred_signs_reg_all_plot,self.pred_signs_all_plot,price = False, sign = True)
          
        return self.pred_signs_reg_all_plot,self.pred_signs_all_plot
    
    
    def get_online_pred_price(self, plot = False):
        
        if plot == True:
            self.__plt_online_predictions(self.pred_prices_reg_all_plot,self.pred_prices_all_plot,price = True, sign = False)
            
        return self.pred_prices_reg_all_plot,self.pred_prices_all_plot
    
    
    def get_sqrt_impact_law(self, plot):
        
        self.__plt_sqrt_impact_law(plot)

    